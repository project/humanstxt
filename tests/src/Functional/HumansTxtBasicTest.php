<?php

namespace Drupal\Tests\humanstxt\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Core\Url;

/**
 * Tests basic functionality of configured humans.txt files.
 *
 * @group Humans.txt
 */
class HumansTxtBasicTest extends BrowserTestBase {

  /**
   * Provides the default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['humanstxt', 'user'];

  /**
   * User with proper permissions for module configuration.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * User with content access.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $normalUser;

  /**
   * HTML Link to a Humans.txt file.
   *
   * @var string
   */
  protected $fileLink;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    // Create users.
    $this->adminUser = $this->drupalCreateUser(['administer humans.txt']);
    $this->normalUser = $this->drupalCreateUser(['access content']);
    // Create link.
    $this->fileLink = '<link rel="author" type="text/plain" hreflang="x-default" href="' .
      Url::fromRoute('humanstxt.content', [], ['absolute' => TRUE])->toString() . '">';
  }

  /**
   * Checks if a non-administrative user cannot access to the config page.
   */
  public function testHumansTxtUserNoAccess() {
    $this->drupalGet('/admin/config/development/humanstxt');
    $this->assertResponse(403);
    $this->drupalLogin($this->normalUser);
    $this->drupalGet('/admin/config/development/humanstxt');
    $this->assertResponse(403);
  }

  /**
   * Checks if the header is right.
   */
  public function testHumansTxtHeader() {
    $this->drupalGet('humans.txt');
    $this->assertResponse(200);
    $this->assertHeader('Content-Type', 'text/plain; charset=UTF-8');
  }

  /**
   * Checks if cache tags exists.
   */
  public function testHumansTxtCacheTags() {
    $this->drupalGet('humans.txt');
    $this->assertResponse(200);
    $this->assertCacheTag('humanstxt');
  }

  /**
   * Checks if humans.txt file is delivered with no link in <head>.
   */
  public function testHumansTxtConfigureHumansTxtNoLink() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/development/humanstxt');
    $this->assertResponse(200);
    $test_string = "# Testing Humans.txt {$this->randomMachineName()}";
    $this->submitForm(['humanstxt_content' => $test_string, 'humanstxt_display_link' => FALSE], t('Save configuration'));
    $this->drupalLogout();

    // Test the newly created humans.txt file.
    $this->drupalGet('humans.txt');
    $this->assertResponse(200);
    $content = $this->getSession()->getPage()->getContent();
    $this->assertTrue($content == $test_string, sprintf('Test string: [%s] is displayed in the configured humans.txt file.', $test_string));

    // Test if the link to the object/file is not in HTML <head> section.
    $this->drupalGet('/admin/config/development/humanstxt');
    $tags = $this->getSession()->getPage()->getHtml();
    $this->assertNotContains($this->fileLink, $tags, sprintf('Humans.txt link: [%s] is not shown in the -head- section.', $this->fileLink));
  }

  /**
   * Checks if humans.txt file is delivered with link in <head>.
   */
  public function testHumansTxtConfigureHumansTxtWithLink() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config/development/humanstxt');
    $this->assertResponse(200);
    $test_string = "# Testing Humans.txt {$this->randomMachineName()}";
    $this->submitForm(['humanstxt_content' => $test_string, 'humanstxt_display_link' => TRUE], t('Save configuration'));
    $this->drupalLogout();

    // Test the newly created humans.txt file.
    $this->drupalGet('humans.txt');
    $this->assertResponse(200);
    $content = $this->getSession()->getPage()->getContent();
    $this->assertTrue($content == $test_string, sprintf('Test string: [%s] is displayed in the configured humans.txt file.', $test_string));

    // Test if the link to the object/file is in HTML <head> section.
    $this->drupalGet('/admin/config/development/humanstxt');
    $tags = $this->getSession()->getPage()->getHtml();
    $this->assertContains($this->fileLink, $tags, sprintf('Humans.txt link: [%s] is shown in the -head- section.', $this->fileLink));
  }

}
