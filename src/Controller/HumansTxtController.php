<?php

namespace Drupal\humanstxt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Cache\CacheableResponse;

/**
 * Class HumansTxtController.
 *
 * HumansTxtController is a base class for handling the humanstxt route.
 * Returns the final humans.txt output as an object with a text/plain type.
 *
 * @package Drupal\humanstxt\Controller
 * @access public
 * @see https://www.drupal.org/project/humanstxt
 */
class HumansTxtController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Humanstxt module 'humanstxt.settings' configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $humanstxtConfig;

  /**
   * Serves the configured humans.txt file.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The humans.txt file is a response object with 'text/plain' type.
   */
  public function content() {
    // Extract the config object from the ControllerBase parent class.
    $this->humanstxtConfig = $this->config('humanstxt.settings');
    // Get value from config.
    $content = $this->humanstxtConfig->get('content');
    // Create a new response.
    $response = new CacheableResponse($content, Response::HTTP_OK, ['content-type' => 'text/plain']);
    // Add cache metadata from the response.
    $meta_data = $response->getCacheableMetadata();
    $meta_data->addCacheTags(['humanstxt']);
    return $response;
  }

}
