<?php

namespace Drupal\humanstxt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Class HumanstxtAdminSettingsForm implements the Humanstxt Settings Form.
 *
 * @package Drupal\humanstxt\Form
 * @access public
 * @see \Drupal\Core\Form\ConfigFormBase
 */
class HumansTxtAdminSettingsForm extends ConfigFormBase {

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'humanstxt_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'humanstxt.settings',
    ];
  }

  /**
   * Build the Humans.txt Config form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Getting the config default values.
    // Always last values or initial by default if post install.
    $config = $this->config('humanstxt.settings');
    $content = $config->get('content');
    $display_link = $config->get('display_link');

    // Building the Form.
    $form['humanstxt_about'] = [
      '#type' => 'item',
      '#markup' => $this->t('Add here the information about the different
                   people who have contributed to building the website, you can
                   find more info in <a href="@humanstxt">humanstxt.org</a> and
                   use <a href="@humanstxt_file">this file</a> as base file.',
                   [
                     '@humanstxt' => 'http://humanstxt.org',
                     '@humanstxt_file' => 'http://humanstxt.org/humans.txt',
                   ]
      ),
    ];

    $form['humanstxt_content'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Content of Humans.txt'),
      '#description' => $this->t('Fill the area following the pattern.'),
      '#default_value' => $content,
      '#cols' => 60,
      '#rows' => 20,
      '#wysiwyg' => FALSE,
    ];

    $form['humanstxt_display_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Insert link to Humans.txt file'),
      '#description' => $this->t('By activating this field you will make
                        Humans.txt file linked from the head section of the
                        HTML code.'),
      '#default_value' => $display_link,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $content_submit = $form_state->getValue('humanstxt_content');
    $display_link_submit = $form_state->getValue('humanstxt_display_link');

    // Set the new values in the config object of the module.
    $this->config('humanstxt.settings')
      ->set('content', $content_submit)
      ->set('display_link', $display_link_submit)
      ->save();

    // Delete former cache tags.
    Cache::invalidateTags(['humanstxt']);
    parent::submitForm($form, $form_state);
  }

}
